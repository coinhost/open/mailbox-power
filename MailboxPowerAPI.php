<?php
class MailboxPowerAPI {
    const API_URL = 'https://www.mailboxpower.com/api/v3';
    private $curl;
    private $apikey;
    public function __construct($apikey) { 
		$this->apikey = $apikey; 
		$this->curl = new Curl\Curl(); 
	}
    public function __destruct() { 
		$this->curl->close(); 
	}
    protected function request($endpoint, $data = [], $method = 'get') {
        $this->curl->reset();
        $this->curl->setHeader('APIKEY', $this->apikey);
        foreach ([
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_FOLLOWLOCATION => true
        ] as $opt => $val) 
			$this->curl->setOpt($opt, $val);
        try {
            $this->curl->{$method}(self::API_URL . $endpoint, $data);
        } catch (Exception $ex) {
            print_r([$method, $endpoint, $data, $this->curl, $ex]);
            return false;
        }
        return json_decode($this->curl->response, false);
    }
    public function contactList() { 
		return $this->request('/contacts'); 
	}
    public function contactDelete($contactId) { 
		return $this->request('/contacts', compact('contactId'), 'delete'); 
	}
    public function contactCreate($contact) { 
		return $this->request('/contacts', $contact, 'post'); 
	}
    public function contactUpdate($contact) { 
		return $this->request('/contacts', $contact, 'put'); 
	}
    public function groupList() { 
		return $this->request('/groups'); 
	}
    public function groupRename($groupId, $groupName) { 
		return $this->request('/groups?'. http_build_query(compact('groupId','groupName')), [], 'put'); 
	}
    public function groupCreate($groupName, $linkmessage = "") { 
		return $this->request('/groups', compact('groupName','linkmessage'), 'post'); 
	}
    public function groupDelete($groupId) { 
		return $this->request('/groups', compact('groupId'), 'delete'); 
	}
    public function groupListContacts($groupId) { 
		return $this->request('/group/contacts', compact('groupId')); 
	}
    public function groupAddContact($groupId, $contactId) { 
		return $this->request('/group/contacts', compact('groupId','contactId'), 'post'); 
	}
    public function groupDeleteContact($groupId, $contactId) { 
		return $this->request('/group/contacts', compact('groupId','contactId'), 'delete'); 
	}
    public function automations() { 
		return $this->request('/automations'); 
	}
    public function getCurl() { 
		return $this->curl; 
	}
}