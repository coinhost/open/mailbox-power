<?php
include_once('mailbox-power.php');
use Illuminate\Database\Capsule\Manager as Capsule;

add_hook('ClientAdd', 1, function($vars) {
    foreach (['api_key', 'group_id'] as $var)
        $$var = Capsule::table('tbladdonmodules')
            ->where('setting', $var)
            ->where('module', 'mailbox-power')
            ->value('value');
    $api = new MailboxPowerAPI($api_key);
    extract($vars);
    $contact = [
        'firstName' => $firstname,
        'lastName' => $lastname,
        'email' => $email,
        'street' => $address1,
        'street2' => $address2,
        'city' => $city,
        'state' => $state,
        'postalcode' => $postcode,
        'country' => $country,
        'phone' => $phonenumber
    ];
    $contactId = $api->contactCreate($contact);
    print_r($contactId);
    print_r($api->groupAddContact($group_id, $contactId->ID));
});