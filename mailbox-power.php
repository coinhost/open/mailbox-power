<?php
foreach (['vendor/autoload', 'MailboxPowerAPI'] as $inc)
    include_once(__DIR__.'/'.$inc.'.php');

function mailbox_power_config() {
    return([
		"name" => "MailBoxPower Integration",
		"description" => "This integrated client database with mailbox power api",
		"version" => "1.0",
		"author" => "maxim@coinhost.io",
		"fields" => [
			"api_key" => [
				"FriendlyName" => "API Key",
				"Type" => "text",
				"Size" => "27",
				"Description" => "MailBox Power API Key",
				"Default" => ""
			],
            "group_id" => [
                "FriendlyName" => "Group ID",
                "Type" => "text",
                "Size" => "27",
                "Description" => "Mailbox Group ID to import contacts to",
                "Default" => ""
            ]
		]
	]);
}
